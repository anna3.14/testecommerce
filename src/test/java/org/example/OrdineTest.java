package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.example.Stato.SPEDITO;
import static org.junit.jupiter.api.Assertions.*;

class OrdineTest {
    Ordine ordine1;
    Prodotto prodotto1;
    Prodotto prodotto2;
    Prodotto prodotto3;

    @BeforeEach
    void setUp(){
        prodotto1=new Prodotto("3456","scarpe",34,10);
        prodotto2=new Prodotto("3976","cappello",12,2);
        prodotto3=new Prodotto("1976","maglia",15,3);
        ordine1=new Ordine();
    }

    @Test
    void testAggiuntaRimozione(){
        ordine1.aggiungiProdotto(prodotto1,1);
        ordine1.aggiungiProdotto(prodotto2,1);
        assertEquals(2,ordine1.getProdotti().size());
        ordine1.rimuoviProdotto(prodotto1);
        assertEquals(1,ordine1.getProdotti().size());

    }

    @Test
    void aggiornaOrdine(){
        ordine1.setStato(Stato.IN_ATTESA);
        ordine1.aggiornaStato(SPEDITO);
        assertEquals(SPEDITO,ordine1.getStato());
    }

    @Test
    void testCostoTotale(){
        ordine1.aggiungiProdotto(prodotto1,1);
        ordine1.aggiungiProdotto(prodotto2,1);
        assertEquals(46.0,ordine1.ottieniTotaleOrdine());
    }


}