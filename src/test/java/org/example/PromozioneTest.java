package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PromozioneTest {
    Ordine ordine1;
    Prodotto prodotto1;
    Prodotto prodotto2;
    Prodotto prodotto3;
    Promozione promozione;

    @BeforeEach
    void setUp(){
        prodotto1=new Prodotto("3456","scarpe",34,10);
        prodotto2=new Prodotto("3976","cappello",12,2);
        prodotto3=new Prodotto("1976","maglia",15,3);
        ordine1=new Ordine();
        promozione=new Promozione("ACD145","codice promo natale",0.1);
    }
    @Test
    void testApplicaPromozione(){
        ordine1.aggiungiProdotto(prodotto1,1);
        assertEquals(3.4,promozione.applicaPromozione(ordine1));
    }



}