package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarrelloTest {
    Carrello carrello;
    Prodotto prodotto1;
    Prodotto prodotto2;
    Prodotto prodotto3;
    Promozione promozione;

    @BeforeEach
    void setUp(){
        promozione=new Promozione();
        prodotto1=new Prodotto("3456","scarpe",34,10);
        prodotto2=new Prodotto("3976","cappello",12,2);
        prodotto3=new Prodotto("1976","maglia",15,3);
        carrello=new Carrello(promozione);
    }

    @Test
    void testAggiuntaRimozione(){
        carrello.aggiungiProdotto(prodotto1,1);
        carrello.aggiungiProdotto(prodotto2,1);
        assertEquals(2,carrello.getProdotti().size());
        carrello.rimuoviProdotto(prodotto1);
        assertEquals(1,carrello.getProdotti().size());

    }

    @Test
    void testApplicazionePromozione(){
        carrello.aggiungiProdotto(prodotto1,1);
        promozione.setPercentualeSconto(0.1);
        assertEquals(3.4,carrello.calcolaTotaleCarrello());
    }

}