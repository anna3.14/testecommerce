package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestioneInventarioTest {
    GestioneInventario inventario;
    Ordine ordineIn;
    Ordine ordineOut;
    Prodotto prodotto1;
    Prodotto prodotto2;
    Prodotto prodotto3;

    @BeforeEach
    void setUp(){
        prodotto1=new Prodotto("3456","scarpe",34,10);
        prodotto2=new Prodotto("3976","cappello",12,2);
        prodotto3=new Prodotto("1976","maglia",15,3);
        ordineIn=new Ordine();
        ordineOut=new Ordine();
        inventario=new GestioneInventario();
    }

    @Test
    void verificaAggiornamentoStock(){
        ordineOut.aggiungiProdotto(prodotto1,2);
        inventario.aggiornaQuantita(ordineOut);
        assertEquals(8,prodotto1.getQuantitaInventario());
        ordineIn.aggiungiProdotto(prodotto1,2);
        inventario.aggiornaStock(ordineIn);
        assertEquals(10,prodotto1.getQuantitaInventario());
        assertTrue(inventario.veridicaDisponibilità(prodotto1));

    }

}