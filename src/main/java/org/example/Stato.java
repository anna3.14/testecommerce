package org.example;

public enum Stato {
    SPEDITO,
    IN_ATTESA,
    CONSEGNATO,
}
