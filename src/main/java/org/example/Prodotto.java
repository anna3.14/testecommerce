package org.example;

public class Prodotto {
    private String id;
    private String nome;
    private double prezzo;
    private int quantitaOrdinata;
    private int quantitaInventario;


    //Costruttore
    public Prodotto(String id, String nome, double prezzo, int quantitaInventario){
        this.nome=nome;
        this.id=id;
        this.prezzo=prezzo;
        this.quantitaInventario=quantitaInventario;
        this.quantitaOrdinata=0;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantitaInventario() {
        return quantitaInventario;
    }

    public void setQuantitaInventario(int quantitaInventario) {
        this.quantitaInventario = quantitaInventario;
    }


    //Metodo per verificare la disponibilità in inventario: quantitaInventario
    public void controlloQuantitaInventario(){
        if(quantitaInventario<=0){
            System.out.println("Prodotto non disponibile in stock");
        }
    }

    public int getQuantitaOrdinata() {
        return quantitaOrdinata;
    }

    public void setQuantitaOrdinata(int quantitaOrdinata) {
        this.quantitaOrdinata = quantitaOrdinata;
    }
}

