package org.example;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ordine {
    private String idOrdine;
    private int numeroProdotto;
    Map<Prodotto,Integer> prodotti; //prodotto-quantità
    private Stato stato;
    private double totaleOrdine;

    //Costruttore
    public Ordine(){
        this.idOrdine=null;
        this.totaleOrdine=0;
        prodotti=new HashMap<>();
    }

    //Aggiungi prodotto all'ordine
    public void aggiungiProdotto(Prodotto prodotto, int numeroProdotti){
        if(prodotti.containsKey(prodotto)){
                prodotti.put(prodotto,prodotti.get(prodotto)+numeroProdotti);
            }
        else{
            prodotti.put(prodotto,numeroProdotti); //Se non è ancora nell'ordine
        }
    }

    public void rimuoviProdotto(Prodotto prodotto){
        if(prodotti.containsKey(prodotto)){
            prodotti.remove(prodotto);
        }
        else{
            System.out.println("Non presente nell'ordine");
        }
    }

    //Metodo calcola totale ordine
    public double ottieniTotaleOrdine(){
        double total=0.0;
        for(Map.Entry<Prodotto,Integer> entry:prodotti.entrySet()){
            total=total+(entry.getKey().getPrezzo()*entry.getValue());
        }
        return total;
    }

    //Metodo per aggiornare stato Ordine
    public void aggiornaStato(Stato stato){
        this.stato=stato;
    }


    public String getIdOrdine() {
        return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public int getNumeroProdotto() {
        return numeroProdotto;
    }

    public void setNumeroProdotto(int numeroProdotto) {
        this.numeroProdotto = numeroProdotto;
    }

    public Stato getStato() {
        return stato;
    }

    public void setStato(Stato stato) {
        this.stato = stato;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = totaleOrdine;
    }
    public Map<Prodotto,Integer> getProdotti(){
        return prodotti;
    }
    public void setProdotti(Map<Prodotto,Integer> prodotti){
        this.prodotti=prodotti;
    }
}
