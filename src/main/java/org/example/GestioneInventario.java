package org.example;

import java.util.HashMap;
import java.util.Map;

public class GestioneInventario {
    Map<Prodotto,Integer> prodottiStock;

    //Costruttore
    public GestioneInventario(){
        Map<Prodotto,Integer> prodottiStock=new HashMap<>();
        this.prodottiStock=prodottiStock;
    }

    //Aggiorna quantità per prodotto in inventario dopo un ordine in uscita
    public void aggiornaQuantita(Ordine ordine) {
        for (Map.Entry<Prodotto, Integer> entry : ordine.getProdotti().entrySet()) {
            int numero = entry.getValue();
            int numeroProdotto = entry.getKey().getQuantitaInventario() - numero;
            entry.getKey().setQuantitaInventario(numeroProdotto);
            prodottiStock.put(entry.getKey(), numeroProdotto);
        }
    }

    //Verifica disponibilità
    public boolean veridicaDisponibilità(Prodotto prodotto){
        boolean risultato=false;
        if(prodotto.getQuantitaInventario()>0){
            System.out.println("Articolo disponibile in stock");
            risultato=true;
        }
        else{
            System.out.println("Non disponibile in stock");

        }
        return risultato;
    }

    //Metodo per gestire gli ordini di rifornimento
    public void aggiornaStock(Ordine ordineRifornimento){
        for(Map.Entry<Prodotto,Integer> entry:ordineRifornimento.getProdotti().entrySet()){
            int numero=entry.getValue();
            int numeroProdotto=entry.getKey().getQuantitaInventario()+numero;
            entry.getKey().setQuantitaInventario(numeroProdotto);
            prodottiStock.put(entry.getKey(),numeroProdotto);
        }
    }




}
