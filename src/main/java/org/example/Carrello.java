package org.example;

import java.util.HashMap;
import java.util.Map;

public class Carrello {
    private Map<Prodotto,Integer> prodotti;
    private Promozione promozione;


    //Costruttore vuoto
    public Carrello(Promozione promozione){
        Map<Prodotto,Integer> prodotti=new HashMap<>();
        this.prodotti=prodotti;
        this.promozione=promozione;
    }

    //Costruttore con lista prodotti
    public Carrello(Map<Prodotto,Integer> prodotti, Promozione promozione){
        this.prodotti=prodotti;
        this.promozione=promozione;
    }

    public Map<Prodotto,Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(Map<Prodotto,Integer> prodotti) {
        this.prodotti=prodotti;
    }

    public Promozione getPromozione() {
        return promozione;
    }


    public void setPromozione(Promozione promozione) {
        this.promozione = promozione;
    }

    //metodo per aggiungere prodotto al carrello
    public void aggiungiProdotto(Prodotto prodotto, int numeroProdotti){
        if(prodotti.containsKey(prodotto)){
            prodotti.put(prodotto,prodotti.get(prodotto)+numeroProdotti);
        }
        else{
            prodotti.put(prodotto,numeroProdotti); //Se non è ancora nell'ordine
        }
    }
    //Metodo rimuovi prodotto dal carrello
    public void rimuoviProdotto(Prodotto prodotto){
        if(prodotti.containsKey(prodotto)){
            prodotti.remove(prodotto);
        }
        else{
            System.out.println("Non presente nel carrello");
        }
    }

    //Metodo per calcolare il totale
    public double calcolaTotaleCarrello(){
        double totale=0;
        for(Map.Entry<Prodotto,Integer> entry:prodotti.entrySet()){
            totale=totale+(entry.getKey().getPrezzo()* entry.getValue());
        }
        if(this.promozione.getPercentualeSconto()==0){
            totale=totale;
        }
        else{
            totale=totale*promozione.getPercentualeSconto();
        }
        return (double)Math.round(totale * 10d) / 10d;
    }
}
