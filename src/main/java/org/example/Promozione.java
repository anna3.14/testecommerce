package org.example;

public class Promozione {
    private String codicePromozione;
    private String descrizione;
    private double percentualeSconto; //Espressa in decimale (es 0.7)

    //Costruttore
    public Promozione(String codicePromozione,String descrizione,double percentualeSconto){
        this.descrizione=descrizione;
        this.codicePromozione=codicePromozione;
        this.percentualeSconto=percentualeSconto;

    }
    public Promozione(){

    }

    public double getPercentualeSconto() {
        return percentualeSconto;
    }

    public void setPercentualeSconto(double percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }

    public String getCodicePromozione() {
        return codicePromozione;
    }

    public void setCodicePromozione(String codicePromozione) {
        this.codicePromozione = codicePromozione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    //Metodo per applicare promozione ad ordine
    public double applicaPromozione(Ordine ordine){
        double risultato=ordine.ottieniTotaleOrdine()*percentualeSconto;
        return (double)Math.round(risultato * 10d) / 10d;
    }
}
